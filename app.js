let myLeads = [];
const btnSave = document.getElementById("btn-save");
const btnDel = document.getElementById("btn-del");
const btnTab = document.getElementById("btn-tab");
const INPUTFIELD = document.getElementById("input-field");
const LISTLINKS = document.getElementById("list-links");
const LOCALLEADS  = JSON.parse(localStorage.getItem("Leads"));

//Check the leads on the localStorage
if (LOCALLEADS){
    myLeads = LOCALLEADS;
    render(myLeads);
};

//Save tab and working with chrome api
btnTab.addEventListener("click", () => {
    chrome.tabs.query({active: true, currentWindow: true}, (tabs) => {             
        myLeads.push(tabs[0].url);
        localStorage.setItem("Leads", JSON.stringify(myLeads));
        render(myLeads);
    });
});

//Place them to the screen
function render(leads){
    let myLinks = "";
    for(let i = 0; i < leads.length; i++){
        myLinks += `<li><a href="${leads[i]}" target="_blank">${leads[i]}</a></li>`;
    };

    LISTLINKS.innerHTML = myLinks;
};

//Delete the leads for everything
btnDel.addEventListener("dblclick", () => {
    myLeads = [];
    localStorage.clear();
    render(myLeads); 
});

//Function to save and clear leads
btnSave.addEventListener("click" , () => {
    myLeads.push(INPUTFIELD.value);
    INPUTFIELD.value = "";
    localStorage.setItem("Leads", JSON.stringify(myLeads));
    render(myLeads);
});
